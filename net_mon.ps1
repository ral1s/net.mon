cls
Write-host "-------------------------------------------"
Write-host "  _  _ ___ _____       __  __  ___  _  _ "
Write-host " | \| | __|_   _|     |  \/  |/ _ \| \| |"
Write-host " |  .` | _|  | |    _  | |\/| | (_) |  .` |"
Write-host " |_|\_|___| |_|   (_) |_|  |_|\___/|_|\_| "
Write-host "-------------------------------------------"
Write-host ""
$Server = Read-Host -Prompt 'Co si chcete pingnout? (napr.:8.8.8.8)'
# $Logname = Read-Host -Prompt 'Vystupni soubor pro chybova hlaseni (napr. log.txt)'
$Logname = "log.txt"
$Echocount = Read-Host -Prompt 'Pocet ECHO: '
$Timeout = Read-Host -Prompt 'Jak dlouhy ma byt timeout (s): '
Write-host "--------------------------------------------"


$StartDate = (Get-Date).ToString()
$Loop = 1
Write-host "Pinguji....  $Server , $StartDate , $Echocount ECHO pozadavku, Timeout $Timeout s"
Write-output "----------------------------------------------------------------------------------------">> $Logname
Write-output "NET.MON log mereni" >> $Logname
Write-output "zacatek monitoringu: $StartDate">> $Logname
Write-output "----------------------------------------------------------------------------------------">> $Logname
Write-output "">> $Logname
Write-output "Pinguji.... $Server , $StartDate , $Echocount ECHO pozadavku, Timeout $Timeout s" >> $Logname
Write-output "" >> $Logname
Write-output "Vypis neuspesnych spojeni: " >>$Logname
Write-output "--------------------------------------------"
Write-output "Provadim mereni..."
Write-output ""
Write-host "Ukoncit CTRL + C."

While ($Loop -gt 0)
{
Ping -n $Echocount $Server | find "Packets: Sent = $Echocount, Received = $Echocount, Lost = 0 (0% loss)" > $null
Start-Sleep -s $Timeout

If ($Lastexitcode -ne 0)
{
$Date = (Get-Date).ToString()
Write-output "Neuspech  $Server , $Date" >> $Logname
}
}